\documentclass{article}
\usepackage{xcolor}
\usepackage{hyperref}

\usepackage[left=1.8cm,top=1.7cm,right=1.8cm,bottom=1.72cm,nohead]{geometry}

\usepackage[scaled]{helvet}
\usepackage{sectsty}
\allsectionsfont{\bfseries\sffamily}

\usepackage{listings}
\lstset{
    frame=single,
    showstringspaces=false,
    breaklines=true,
    postbreak=\raisebox{0ex}[0ex][0ex]{\ensuremath{\color{red}\hookrightarrow\space}}
}
\lstdefinestyle{BashInputStyle}{
  language=bash,  morekeywords={mkdir,ls,rm,mv,cp,date,hostname,whoami,zip,unzip,rmdir,curl,grep,head,tail,less,vim,which},
  basicstyle=\small\sffamily,
%  numbers=left,
%  numberstyle=\tiny,
%  numbersep=3pt,
  frame=tb,
  columns=fullflexible,
  backgroundcolor=\color{yellow!20},
  linewidth=0.9\linewidth,
  xleftmargin=0.1\linewidth,
  literate={-}{-}1,
}

\lstdefinestyle{CeeProg}{
 language=C,
  basicstyle=\small\sffamily,
%  numbers=left,
%  numberstyle=\tiny,
%  numbersep=3pt,
  frame=tb,
  columns=fullflexible,
  backgroundcolor=\color{green!20},
  linewidth=0.9\linewidth,
  xleftmargin=0.1\linewidth
}

\lstdefinestyle{MakeProg}{
 language=make,
  basicstyle=\small\sffamily,
%  numbers=left,
%  numberstyle=\tiny,
%  numbersep=3pt,
  frame=tb,
  columns=fullflexible,
  backgroundcolor=\color{red!20},
  linewidth=0.9\linewidth,
  xleftmargin=0.1\linewidth
}

\usepackage{menukeys}

\begin{document}

\noindent
{\Large \textsf{\textbf{SICSA Seven Hacks Workshop}}}

\bigskip


There are seven challenges presented below. You will be working in
small teams, and focusing on different subsets of the challenge set. For
each challenge you tackle, your team should should:
\begin{enumerate}
\item discuss exactly what steps you should take to solve the
  challenge.
\item explore which command-line utilities you might use to address
  each step.
\item install and experiment with these utilities.
\item write a script to solve the challenge --- consider the trade-off
  between \emph{elegance} and \emph{brevity} in your script.
\item commit (and push) your script to your branch in the git repo.
\end{enumerate}


\section*{Challenge 1: Paper thumbnail page}

Think \textsf{instagram} for your publications. Suppose you have a set of PDF
papers --- for instance your PhD publications, or perhaps your reading
list.
Your aim is to generate a single HTML page that shows a png thumbnail
image for the first page of all the papers. The papers should be ordered from most
recent first through to oldest last. Assume that filesystem timestamps
can be used to determine each paper's age.

Your solution should be a short bash script that takes a directory name
as an input argument. You should generate thumbnails for all PDFs in
this directory. The bash script should produce an HTML output file
with \texttt{IMG} tags for all the thumbnails, ideally rendered to
flow neatly on the HTML page.

\emph{Ambitious extension:} The size of each thumbnail could be
proportional to the number of Google Scholar citations this paper has received.


\section*{Challenge 2: Lovely LoC graph}


How many different languages do you use, while doing PhD
research. Perhaps you have a favourite programming language---whether
C++, Java, or something more esoteric like Haskell. It's almost
certain you use other languages for `glue' code, maybe Perl, Python or
bash.
Then there is \LaTeX source, for your dissertation.

This challenge involves generating a graph to show how many lines of
code (LoC) you have written. You should create a bar chart. The
$y$-axis measures LoC. There is a separate bar for each source
language. The languages are arranged in alphabetical order on the
$x$-axis.

Generate a script to produce this graph. The script should take a
project root directory as an input parameter. You could hard-code the
source languages (and corresponding file extensions?) into the
script. If you were very clever, you might use Mime type utilities
instead to identify source files.
The output should be a bar chart graphic, saved as a PDF file.

\emph{Ambitious extension:} Consider generating a line-graph
instead. The $y$-axis still records LoC. Now the $x$-axis indicates
the date. There will be a line for each source code language, with
points at different dates. This might require a git repository or
similar, so you could query historical data. Alternatively you could
set up a \texttt{cron} job to take measurements every day. This way,
you end up with something like a \emph{thesisometer}, measuring your
progress during your PhD.


\section*{Challenge 3: Wordcloud history}

Which commands do you use most frequently, at
your command prompt? Your history file should tell you. My history
file is located at \verb+~/.bash_history+ but your file might be
somewhere else.
Your task is to generate a wordcloud image of your commands, where the
size of each word is related to its frequency in your history

I used the Python wordcloud library to generate my graphic output as a
png. I had to run

\begin{lstlisting}[style=BashInputStyle]
    $ pip install wordcloud
\end{lstlisting}

which also installs \textsf{matplotlib} and \textsf{numpy}.
You can either write a Python program to generate the wordcloud, or
you can use the command-line utility installed on your PATH by pip --- this is
called \textsf{wordcloud\_{}cli.py}, and it has plenty of options. See 
\begin{lstlisting}[style=BashInputStyle]
    $ wordcloud_cli.py --help
\end{lstlisting}

for details.

\emph{Ambitious extension:} The wordcloud utility allows you to
\emph{suppress} some words since they are very common. These are
called stop-words. What would your stop-words be for your history
file. You might also think about removing individual file names or
other command options/switches from the history before you generate
the wordcloud.


\section*{Challenge 4: Log file mining}

Your system keeps sooo many log files. The problem is, it takes a long
time to read and digest these logs. What we really want is a simple
high-level summary that shows us `interesting' events.

Select a log file. This might be \textsf{dmesg} or
\textsf{/var/log/messages}. Perhaps grep for segfaults, or dodgy ssh
logins.

If you want to download our example problems, get the tarball from
the git repo.
Grep for lines with \texttt{sshd} and then filter for those containing 
\texttt{op=login}. How many distinct IP addresses are involved in
logins? How many of these are failed logins --- probably dodgy. Can
you \textit{geolocate} these IP addresses and work out which countries
potential hackers are most likely to come from?
I suggest using the \textsf{geoiplookup} utility to map IP addresses
onto countries.


\section*{Challenge 5: Write a horrible C program}

This is a creative challenge. Your task is to write a \emph{bad} C
program.
Here is an example:


\begin{lstlisting}[style=CeeProg]
// how to leak a file handle

void f(char* name) {
    FILE* input = fopen(name, "r");
    // ...
    if (something) return;   // bad
    // ...
    fclose(input);
}
\end{lstlisting}

This program opens a file handle but might not close it.
Other common C problems include:
\begin{enumerate}
\item using uninitialized memory or variables
\item use after \texttt{free}
\item double \texttt{free}
\item array out of bounds accesses on stack or heap
\item anything else?
\end{enumerate}

Write a C program that exhibits as many of these problems as
possible. Save it to your team's branch of your repository.




\section*{Challenge 6: Code cleanup}

Find another team's bad C program from challenge 5. Can you identify
all their issues? Can you engineer fixes for these issues? Use tools
like \textsf{splint}, \textsf{valgrind} and your C compiler's \texttt{-Wall} flag.


\section*{Challenge 7: Crawling over your network}

You can use \texttt{ifconfig} or \texttt{ip addr show}
to determine your network address. On
Dundee eduroam, you might be assigned a \texttt{10.202/16} address. 

We want to find out whether there are other machines on the same
subnet. You should be able to do a \textit{broadcast ping} to find
machines that reply to your ping messages. 

Hopefully there are lots of responsive machines. Now we can use the
\textsf{nmap} tool to find out more about these machines. This tool
will tell us what kind of OS is running on each machine, and which
network ports are open. Try something like:

\begin{lstlisting}[style=BashInputStyle]
nmap -O 10.202.101.77
\end{lstlisting}

(or whatever IP address you find). You can specify a subnet with
something like \texttt{10.202.101/24}.
How many machines can you see? What are their operating systems?

\emph{Ambitious extension:} The challenge is to find a machine you might be able to compromise ---
although we don't want you to do that, of course. Apparently there are
some Raspberry Pi devices on the net today. They are 
running Raspbian with default username/passwords. Can you find them?


\end{document}
